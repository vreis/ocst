open Engine
open System
open Events
open Easy
open Metrics
open Obandit

let default_policies = 
  [BatList.assoc "fcfs" Metrics.criteriaList;BatList.assoc "spf" Metrics.criteriaList]

type algoState = {
  bandit : Obandit.banditPolicy;       (*the mab*)
  last_t : int BatOption.t;            (*the last round time*)
  last_s : System.system BatOption.t ; (*the last system state*)
  starts : (int*int) list              (*list of id,start_time started during round *)
}

module type BanditSelectorParam = sig
  val period : int
  val cost : System.job_table -> Engine.history -> float
  val policyList : (module Criteria) list
  val initial_bandit : Obandit.banditPolicy
end

module type BanditSelectorSecondaryParam = sig
  val policyList : (module Criteria) list
end

let listReorderFunctions lc systemModule = 
  let f m = 
    let module M = MakeGreedyPrimary((val m:Criteria))((val systemModule:SchedulerParam))
    in M.reorder
  in List.map f lc

let listSecondaryReorderFunctions lc systemModule = 
  let f m = 
    let module M = MakeGreedySecondary((val m:Criteria))((val systemModule:SchedulerParam))
    in M.pick
  in List.map f lc

module MakeBanditSelector
(BSP:BanditSelectorParam)
(B:Obandit.Bandit with type bandit = Obandit.banditPolicy)
(SP:SchedulerParam)
: Primary with type algo_state = algoState 
= struct
  let desc = "bandit"

  type algo_state = algoState
  let initial_algo_state = 
    { bandit = BSP.initial_bandit;
      last_t = None;
      last_s = None;
      starts = [];
    }
  let allReorders = listReorderFunctions BSP.policyList (module SP:SchedulerParam)

  let update_state algo_state now decision backfilled = 
    { algo_state with
          starts = algo_state.starts @ (List.map (fun x -> (x,now)) (decision@backfilled))}

  let reorder ~system:s ~now:now ~log:log ~algo_state:algo_state =
    match algo_state.last_t with
      |None -> 
        let _,bandit = B.step algo_state.bandit 0.
        in let log,dec,_ = (List.nth allReorders bandit.a) 
            ~system:s ~now:now ~log:log ~algo_state:()
        in log,
           dec, 
           { bandit = bandit;
             last_t = Some now;
             last_s = Some s;
             starts = []; }
      |Some last_t -> 
        let t = last_t + BSP.period
        in if now < t then
          let log,dec,_ = (List.nth allReorders algo_state.bandit.a) 
              ~system:s ~now:now ~log:log ~algo_state:()
          in log, dec, algo_state
        else
          let reward =
            let metric = BSP.cost SP.jobs 
            in let now_hist = List.map (fun x -> (x,t)) s.waiting
            and previous_hist = 
              let last_s = BatOption.get algo_state.last_s
              in List.map (fun x -> (x,last_t)) last_s.waiting
            in let cost_now                    = metric now_hist
               and cost_last                   = metric previous_hist
               and cost_during_real            = metric algo_state.starts
               and cost_during_nolaunch        = metric ((List.map (fun (x,_)-> (x,t))) algo_state.starts)
               and cost_during_immediatelaunch = 
                 let sub i = (Hashtbl.find SP.jobs i).r
                 in metric ((List.map (fun (x,_)-> (x,sub x))) algo_state.starts)
            in let cost_best = cost_last +. cost_during_immediatelaunch
               and cost_worst = cost_now +. cost_during_nolaunch
            in let cost_real = cost_now +. cost_during_real
            in (cost_worst -. cost_real) /. (max 0.001 (cost_worst -. cost_best))
          in let _,bandit = B.step algo_state.bandit reward
          (*in let () = Printf.printf "the bandit choice is %d and the list length is %d\n" bandit.a (List.length allReorders)*)
          in let log,dec,_ = (List.nth allReorders bandit.a) 
              ~system:s ~now:now ~log:log ~algo_state:()
          in log,
             dec,
             { algo_state with
               bandit = bandit;
               last_t = Some t;
               last_s = Some s;
               starts = [] }
end



module MakeSecondary
    (BSSP:BanditSelectorSecondaryParam)
    (SP:SchedulerParam)
  : Secondary
 with type algo_state = algoState =
struct
  type algo_state = algoState
  let allSecondaryReorders = listSecondaryReorderFunctions BSSP.policyList (module SP:SchedulerParam)

  let pick
      ~system:s
      ~now:now
      ~backfillable:bfable
      ~reservationWait:restime
      ~reservationID:resjob
      ~reservationFree:free'
      ~freeNow:free 
      ~algo_state:algo_state =
           (List.nth allSecondaryReorders algo_state.bandit.a) 
            ~system:s
            ~now:now
            ~backfillable:bfable
            ~reservationWait:restime
            ~reservationID:resjob
            ~reservationFree:free'
            ~freeNow:free 
            ~algo_state:()
end
