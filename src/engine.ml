open System

(************************************** History **********************************)

type history = (int*int) list (*list of (id,start_time)*)

(************************************** Engine ***********************************)
(*TODO refactor. Statistics module*)
module type Stat = sig
  type outputStat
  val incStat : int   (* time now*)
  -> int list         (*actual jobs to start now*)
  -> unit
  val getStat : unit -> outputStat
  val getN : unit -> int
  val reset : unit -> unit
end
(*END TODO*)

module type SimulatorParam = sig
  val jobs : job_table
end

(*simulator module*)
module type Simulator = sig
  type algo_state
  val simulate : Events.EventHeap.t -> system -> history ->  log -> algo_state -> (history * log)
end

(*simulator building functor*)
module MakeSimulator
  (Sch:Easy.Scheduler)
  (P:SimulatorParam)
  :Simulator with type algo_state = Sch.algo_state=
struct
  type algo_state = Sch.algo_state
  (*apply some events' effect on the system; this can be optimized.*)
  let executeEvents ~eventList:el system=
    let execute s (e:Events.EventHeap.elem) =
      match e.event_type with
        | Submit -> { s with waiting = e.id::s.waiting }
        | Finish ->
            let q = (Hashtbl.find P.jobs e.id).q
            in { running = List.filter (fun (_,i) -> i!=e.id) s.running;
                 free = s.free + q;
                 waiting=s.waiting; }
    in List.fold_left execute system el

  (*apply some scheduling decisions on the system and the event heap.*)
  let executeDecisions system heap now idList history =
    let jobList = List.map (fun i -> (i,Hashtbl.find P.jobs i)) idList
    in let f (s,h,hist) (i,j) =
      { free = s.free - j.q;
        running = (now,i)::s.running;
        waiting = List.filter (fun x -> not (i=x)) s.waiting;
      }
      ,Events.EventHeap.insert h {time = now+j.p; id=i; event_type=Finish}
      ,(i,now)::hist
    in List.fold_left f (system,heap,history) jobList

  let simulate (eventheap:Events.EventHeap.t) (system:system) (hist:history) (log:log) (initial_algo_state:algo_state) = 
    (*step h s where h is the event heap and s is the system*)
    let rec step syst heap hist log algo_state=
      match Events.EventHeap.unloadEvents heap with
        | Events.EventHeap.EndSimulation -> (hist, log)
        | Events.EventHeap.Events (h, now, eventList) ->
            let s = executeEvents ~eventList:eventList syst
            in let decisions, log, algo_state = Sch.schedule now s log algo_state
            in let s,h,hi = (executeDecisions s h now decisions hist)
            in step s h hi log algo_state
    in step system eventheap hist log initial_algo_state
end

(************************************** Stats ************************************)
