open Metrics

let timesched f x =
  let () = Printf.printf "%s\n" "Simulating.."
  in let start = Unix.gettimeofday ()
  in let res = f x
  in let stop = Unix.gettimeofday ()
  in let () = Printf.printf "Done. Simulation time: %fs\n%!" (stop -. start)
  in res

type copts = {
  swf_in: string;
  swf_out : string option;
  initial_state : string option;
  additional_jobs : string option;
  max_procs : int;
  stats : (module Statistics.Stat) list;
  debug : bool}
let copts swf_in swf_out initial_state additional_jobs max_procs debug seed stats =
  Random.init seed;
  {swf_in; swf_out; initial_state; additional_jobs; max_procs; debug; stats}


let run_simulator ?period:(period=86400) ?state_out:(state_out = None) ?log_out:(log_out=None) copts primsec  job_table max_procs=
  let h,s =
    let heap_before = Events.EventHeap.of_job_table job_table
    in let () = match copts.additional_jobs with
      |None -> ()
      |Some fn -> let jt,_ = Io.parse_jobs fn
    in Hashtbl.iter (fun i j -> Hashtbl.add job_table i j) jt;
      in let s,h = match copts.initial_state with
      |None ->
          let real_mp = max max_procs copts.max_procs
          in System.empty_system real_mp,heap_before
      |Some fn ->
          let s = Io.load_system fn
            in let events =
              let make_event (t,i) : Events.EventHeap.elem=
                { time=(Hashtbl.find job_table i).p+t;
                  id=i;
                  event_type=Events.EventHeap.Finish}
          in List.map make_event s.running
              in let h = List.fold_left Events.EventHeap.insert heap_before events
            in s,h
              in h,s
      in let module SchedulerParam = struct let jobs = job_table end
  in let module PrimSec = (val primsec:Easy.PrimarySecondary)
  in let module Primary = (val PrimSec.prim:Easy.Primary with type algo_state = PrimSec.algo_state)
  in let module Secondary = (val PrimSec.sec:Easy.Secondary with type algo_state = PrimSec.algo_state)
  in let module Scheduler = Easy.MakeEasyScheduler(Primary)(Secondary)(SchedulerParam)
  in let module S =
    Engine.MakeSimulator(Scheduler)(struct include SchedulerParam end)
  in let hist,log = S.simulate h s [] [] Primary.initial_algo_state
  in (Io.hist_to_swf job_table copts.swf_out hist;
      Io.log_to_file log_out Primary.desc log;
      let f s =
        let module M = (val s:Statistics.Stat)
        in M.stat
        in let stv = List.map (fun s -> (f s) job_table hist) copts.stats
      in let sts = String.concat "," (List.map (Printf.sprintf "%0.3f") stv)
        in Printf.printf "%s" sts)

let fixed copts reservation backfill threshold =
  let jt,mp = Io.parse_jobs copts.swf_in
  in let module T = struct let threshold = threshold end 
  in let module SP =  struct let jobs = jt end
  in let module M = Easy.MakeThreshold(T)(Easy.MakeGreedyPrimary((val reservation:Metrics.Criteria))(SP))(SP)
  in let module CSec = (val backfill:Metrics.Criteria)
  in let module M2 = Easy.MakeGreedySecondary(CSec)(SP)
  in let module PrimSec = struct
       type algo_state = unit
         let prim = (module M: Easy.Primary with type algo_state=algo_state)
         let sec = (module M2: Easy.Secondary with type algo_state=algo_state)
     end
  in run_simulator copts (module PrimSec:Easy.PrimarySecondary)  jt mp 

let mixed copts backfill feature_out alpha alpha_threshold alpha_poly alpha_system proba sampling threshold=
  let jt,mp = Io.parse_jobs copts.swf_in
  in if proba then
    let module Pc
    = struct
      let sampling = sampling
      let criterias =
        let f ftlist (param:float list * float list * float list) : Metrics.criteria list = List.map snd ftlist
          in [ BatOption.map (f features_job_plus)                alpha;
               BatOption.map (f features_job_threshold) alpha_threshold;
               BatOption.map (f features_job_advanced)       alpha_poly;
               BatOption.map (f features_system_job)       alpha_system;]
    |> List.filter BatOption.is_some
      |> List.map BatOption.get
      |> BatList.reduce List.append
      let alpha =
        [alpha;alpha_threshold;alpha_poly;alpha_system]
    |> List.filter BatOption.is_some
      |> List.map BatOption.get
      |> List.map BatTuple.Tuple3.first
      |> BatList.reduce List.append
  end
        in let module T = struct let threshold = threshold end 
        in let module M = Easy.MakeThreshold(T)(Easy.MakeProbabilisticPrimary(Pc)(struct let jobs=jt end))(struct let jobs=jt end)
        in let module SP = struct let jobs=jt end
        in let module CSec = (val backfill:Metrics.Criteria)
        in let module M2 = Easy.MakeGreedySecondary(CSec)(SP)
        in let module PrimSec = struct
             type algo_state = unit
               let prim = (module M: Easy.Primary with type algo_state=algo_state)
               let sec = (module M2: Easy.Secondary with type algo_state=algo_state)
           end
        in run_simulator copts (module PrimSec:Easy.PrimarySecondary) jt mp 
  else
    let m = [ BatOption.map (Metrics.makeMixed features_job_plus)               alpha;
              BatOption.map (Metrics.makeMixed features_job_threshold) alpha_threshold;
              BatOption.map (Metrics.makeMixed features_job_advanced) alpha_poly;
              BatOption.map (Metrics.makeMixed features_system_job) alpha_system;]
    |> List.filter BatOption.is_some
      |> List.map BatOption.get
      |> BatList.reduce Metrics.makeSum
        in let module P = (val m:Metrics.Criteria)
        in let module T = struct let threshold = threshold end 
        in let module SP = struct let jobs=jt end
        in let module M = Easy.MakeThreshold(T)(Easy.MakeGreedyPrimary(P)(SP))(SP)
        in let module CSec = (val backfill:Metrics.Criteria)
        in let module M2 = Easy.MakeGreedySecondary(CSec)(SP)
        in let module PrimSec = struct
             type algo_state = unit
               let prim = (module M: Easy.Primary with type algo_state=algo_state)
               let sec = (module M2: Easy.Secondary with type algo_state=algo_state)
           end
  in run_simulator ~log_out:feature_out copts (module PrimSec:Easy.PrimarySecondary) jt mp 

let contextual copts period policies ipc=
  let getcrit m =
    let module M = (val m:Metrics.Criteria)
    in M.criteria
  and getdesc m =
    let module M = (val m:Metrics.Criteria)
    in M.desc
  (*in let () = List.iter (fun x -> Printf.printf "%s" @@ getdesc x) policies*)
  in let jt,mp = Io.parse_jobs copts.swf_in
  in let module P =
      struct
        let period = period
        let policies = List.map getcrit policies
        let ipc = "/tmp/"^ipc^".ipc"
    end
  in let module SP = struct let jobs=jt end
  in let module M = Easy.MakeContextualPrimary(P)(SP)
  in let module M2 = Easy.MakeGreedySecondary(Metrics.FCFS)(SP)
  in let module PrimSec = struct
       type algo_state = unit
         let prim = (module M: Easy.Primary with type algo_state=algo_state)
         let sec = (module M2: Easy.Secondary with type algo_state=algo_state)
     end
  in run_simulator copts (module PrimSec:Easy.PrimarySecondary) jt mp 

let bandit copts cost explo period threshold policies=
  let jt,mp = Io.parse_jobs copts.swf_in
  and k = List.length policies
  in let eta = 
    let n = 
      let mi,ma = 
        let f (i:int) (j:System.job) = function 
            |Some (mi,ma) -> Some begin
                if j.r < mi then j.r,ma
                else if j.r > ma then mi,j.r
                else mi,ma
              end
            |None -> Some (j.r,j.r)
        in BatOption.get (Hashtbl.fold f jt None)
      in (ma-mi) / period
    and k = float_of_int k
    in explo *. sqrt ( 2. *. (log k) /. ((float_of_int n) *. k) )
  in let module T= struct
       let threshold=threshold
     end
  in let module B=Obandit.MakeFixedExp3(struct
         let k = List.length policies
         let eta=eta
       end)
  in let module BSP = struct 
       let period = period
       let cost = 
         let module M = (val cost:Statistics.Stat)
         in M.stat
       let policyList = policies
       let initial_bandit = B.initialBandit
     end 
  in let module BSSP = struct 
       let policyList = policies
     end 
  in let module SP= struct
       let jobs=jt
     end
  in let module M = Easy.MakeThreshold(T)(Bandit.MakeBanditSelector(BSP)(B)(SP))(SP)
  in let module M2 = Bandit.MakeSecondary(BSSP)(SP)
  in let module PrimSec = struct
       type algo_state = M.algo_state
         let prim = (module M: Easy.Primary with type algo_state=algo_state)
         let sec = (module M2: Easy.Secondary with type algo_state=algo_state)
     end
  in run_simulator copts (module PrimSec:Easy.PrimarySecondary) jt mp 

let simfeedback copts cost period threshold policies=
  let jt,mp = Io.parse_jobs copts.swf_in
  in let module T= struct
       let threshold=threshold
     end
  in let module SFP = struct 
       let period = period
       let cost = 
         let module M = (val cost:Statistics.Stat)
         in M.stat
       let policyList = policies
       let threshold = threshold
     end 
  in let module BSSP = struct 
       let policyList = policies
     end 
  in let module JP= struct
       let jobs=jt
     end
  in let module M = Easy.MakeThreshold(T)(Simfeedback.MakeSimFeedbackSelector(SFP)(JP))(JP)
  in let module M2 = Simfeedback.MakeSecondary(BSSP)(JP)
  in let module PrimSec = struct
       type algo_state = M.algo_state
         let prim = (module M: Easy.Primary with type algo_state=algo_state)
         let sec = (module M2: Easy.Secondary with type algo_state=algo_state)
     end
  in run_simulator copts (module PrimSec:Easy.PrimarySecondary) jt mp 

let random copts period threshold policies=
  let jt,mp = Io.parse_jobs copts.swf_in
  and k = List.length policies
  in let module T= struct
       let threshold=threshold
     end
  in let module RSP = struct 
       let period = period
       let policyList = policies
     end 
  in let module RSSP = struct 
       let policyList = policies
     end 
  in let module SP= struct
       let jobs=jt
     end
  in let module M = Easy.MakeThreshold(T)(Randomperiod.MakeRandomSelector(RSP)(SP))(SP)
  in let module M2 = Randomperiod.MakeSecondary(RSSP)(SP)
  in let module PrimSec = struct
       type algo_state = M.algo_state
         let prim = (module M: Easy.Primary with type algo_state=algo_state)
         let sec = (module M2: Easy.Secondary with type algo_state=algo_state)
     end
  in run_simulator copts (module PrimSec:Easy.PrimarySecondary) jt mp 
