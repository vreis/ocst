open Engine
open System
open Events
open Easy
open Metrics

let default_policies = 
  [BatList.assoc "fcfs" Metrics.criteriaList;BatList.assoc "spf" Metrics.criteriaList]

type algoState = {
  estimates : float list;                 (*the estimates*)
  round  : int;                           (*round count*)
  last_t : int BatOption.t;               (*the last round time*)
  last_s : System.system BatOption.t;     (*the last system state*)
  starts : (int*int) list;                (*list of id,start_time started during round *)
  last_a : (module Criteria) BatOption.t; (*the current action*)
  }

module type SimFeedbackParam = sig
  val period : int
  val cost : System.job_table -> Engine.history -> float
  val policyList : (module Criteria) list
  val threshold : int
end

module MakeSimFeedbackSelector
(SFP:SimFeedbackParam)
(SP:SchedulerParam)
: Primary with type algo_state = algoState 
= struct
  let desc = "simulated feedback"
  type algo_state = algoState

  let initial_algo_state = 
    { estimates = BatList.make (List.length SFP.policyList) 0.;
      last_a = None;
      round = 0 ;
      last_t = None;
      last_s = None;
      starts = [];
    }
  let allReorders = Bandit.listReorderFunctions SFP.policyList (module SP:SchedulerParam)

  let update_state algo_state now decision backfilled = 
    { algo_state with
          starts = algo_state.starts @ (List.map (fun x -> (x,now)) (decision@backfilled))}

  let reorder ~system:s ~now:now ~log:log ~algo_state:algo_state = 
    match algo_state.round with
    |0 -> let log,dec,_ = List.hd allReorders
            ~system:s ~now:now ~log:log ~algo_state:()
          in log,
             dec, 
             { algo_state with 
               last_a = Some (List.hd SFP.policyList);
               last_t = Some now;
               last_s = Some s;
               round = 1;
                }
    |round -> 
        let t = (BatOption.get algo_state.last_t) + SFP.period
        in if now < t then
          let log,dec,_ = 
            let module Mr = MakeGreedyPrimary((val (BatOption.get algo_state.last_a):Criteria))(SP)
            in Mr.reorder ~system:s ~now:now ~log:log ~algo_state:()
          in log, dec, algo_state
        else
          let last_s = BatOption.get algo_state.last_s
          in let rewards = 
            let simulate policy = 
              let eventheap = EventHeap.of_period SP.jobs last_s s algo_state.starts
              in let module T = struct let threshold = SFP.threshold end 
              in let module M = Easy.MakeThreshold(T)(Easy.MakeGreedyPrimary((val policy:Metrics.Criteria))(SP))(SP)
              in let module CSec = (val policy:Metrics.Criteria)
              in let module M2 = Easy.MakeGreedySecondary(CSec)(SP)
              in let module Scheduler = Easy.MakeEasyScheduler(M)(M2)(SP)
            in let module S = Engine.MakeSimulator(Scheduler)(SP)
            (*in let () = System.print_system_summary SP.jobs last_s*)
            (*in let () = EventHeap.print_events SP.jobs eventheap *)
            in let hist,_ = S.simulate eventheap last_s [] [] ()
            in SFP.cost SP.jobs hist
            in List.map simulate SFP.policyList
          in let rewards =  BatList.map2 (fun x y -> x +. y) algo_state.estimates rewards 
          in let perflist = BatList.map2 (fun x y -> (x,y)) rewards SFP.policyList
          in let a = 
            let cmp (r,p) (r',p') =  Pervasives.compare r r'
            in snd (fst (BatList.min_max ~cmp:cmp perflist))
          in let module Mr = MakeGreedyPrimary((val a:Criteria))(SP)
          in let log,dec,_ = Mr.reorder ~system:s ~now:now ~log:log ~algo_state:()
          in log,
             dec,
             { algo_state with
               estimates = rewards;
               round = round + 1;
               last_t = Some t;
               last_s = Some s;
               starts = [] ;
               last_a = Some a}
end

module MakeSecondary
    (BSSP:Bandit.BanditSelectorSecondaryParam)
    (SP:SchedulerParam)
  : Secondary
 with type algo_state = algoState =
struct
  type algo_state = algoState
  let allSecondaryReorders = Bandit.listSecondaryReorderFunctions BSSP.policyList (module SP:SchedulerParam)

  let pick
      ~system:s
      ~now:now
      ~backfillable:bfable
      ~reservationWait:restime
      ~reservationID:resjob
      ~reservationFree:free'
      ~freeNow:free 
      ~algo_state:algo_state =
         let module M = MakeGreedySecondary((val (BatOption.get algo_state.last_a):Criteria))(SP)
         in M.pick
           ~system:s
           ~now:now
           ~backfillable:bfable
           ~reservationWait:restime
           ~reservationID:resjob
           ~reservationFree:free'
           ~freeNow:free 
           ~algo_state:()
end
