open Engine
open System
open Events
open Easy
open Metrics

let default_policies = 
  [BatList.assoc "fcfs" Metrics.criteriaList;BatList.assoc "spf" Metrics.criteriaList]

type algoState = {
  a: int;
  last_t : int BatOption.t;
}

module type RandomSelectorParam = sig
  val period : int
  val policyList : (module Criteria) list
end

module type RandomSelectorSecondaryParam = sig
  val policyList : (module Criteria) list
end

let listReorderFunctions lc systemModule = 
  let f m = 
    let module M = MakeGreedyPrimary((val m:Criteria))((val systemModule:SchedulerParam))
    in M.reorder
  in List.map f lc

let listSecondaryReorderFunctions lc systemModule = 
  let f m = 
    let module M = MakeGreedySecondary((val m:Criteria))((val systemModule:SchedulerParam))
    in M.pick
  in List.map f lc

module MakeRandomSelector
(RSP:RandomSelectorParam)
(SP:SchedulerParam)
: Primary with type algo_state = algoState
= struct
  let desc = "random-period"

  type algo_state = algoState
  let initial_algo_state = 
    { a=0;
      last_t=None
     }
  let allReorders = listReorderFunctions RSP.policyList (module SP:SchedulerParam)

  let update_state algo_state now decision backfilled = algo_state

  let reorder ~system:s ~now:now ~log:log ~algo_state:algo_state =
    match algo_state.last_t with
      |None -> 
        let log,dec,_ = (List.nth allReorders algo_state.a) 
            ~system:s ~now:now ~log:log ~algo_state:()
        in log,
           dec, 
           { algo_state with
             last_t = Some now;}
      |Some last_t -> 
        let t = last_t + RSP.period
        in if now < t then
          let log,dec,_ = (List.nth allReorders algo_state.a) 
              ~system:s ~now:now ~log:log ~algo_state:()
          in log, dec, algo_state
        else
          let r = Random.int (List.length allReorders)
          in let log,dec,_ = (List.nth allReorders r) 
              ~system:s ~now:now ~log:log ~algo_state:()
          in log, dec, 
             {algo_state with a=r}
end


module MakeSecondary
    (RSSP:RandomSelectorSecondaryParam)
    (SP:SchedulerParam)
  : Secondary
 with type algo_state = algoState =
struct
  type algo_state = algoState
  let allSecondaryReorders = listSecondaryReorderFunctions RSSP.policyList (module SP:SchedulerParam)

  let pick
      ~system:s
      ~now:now
      ~backfillable:bfable
      ~reservationWait:restime
      ~reservationID:resjob
      ~reservationFree:free'
      ~freeNow:free 
      ~algo_state:algo_state =
           (List.nth allSecondaryReorders algo_state.a) 
            ~system:s
            ~now:now
            ~backfillable:bfable
            ~reservationWait:restime
            ~reservationID:resjob
            ~reservationFree:free'
            ~freeNow:free 
            ~algo_state:()
end
