open System
open BatEnum

let shift=3600*24*7

let sample seed swf_out swf_in  =
  let jt,mp = Io.parse_jobs swf_in
  in let () = Random.init seed
  in let max_user = 
    let f i j m = max m j.u
    in Hashtbl.fold f jt 0
  in let userlist = BatList.of_enum (0 -- (max_user+1))
  in let forward_shifted_users,backward_shifted_users = 
    let f (l,l') u = match Random.int 5 with
      |1 -> (l@[u],l')
      |2 -> (l,l'@[u])
      |_ -> (l,l')
    in List.fold_left f ([],[]) userlist
  in let printer chan = 
     let f i j = 
       let predicate u' u = ( u = u' )
       in if List.exists (predicate j.u) forward_shifted_users then
         Io.printjob_shift (j.r+shift) (Hashtbl.find jt i) i chan
       else if List.exists (predicate j.u) backward_shifted_users then
         Io.printjob_shift (j.r-shift) (Hashtbl.find jt i) i chan
       else
         Io.printjob_shift j.r j i chan
     in Hashtbl.iter f jt
  in Io.wrap_io_out (Some swf_out) printer
