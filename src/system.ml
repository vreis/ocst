open Sexplib.Std

(************************************** Jobs *************************************)
type job = { r:int; p:int; p_est:int; q:int; u:int} (*job data*)
type job_table = (int,job) Hashtbl.t                (*id-indexed table*)

(************************************** System ***********************************)
type system =
    { free: int;                             (*free resources*)
      running: (int*int) list;               (*currently running jobs (startT,id)*)
      waiting : int list;                    (*queud jobs (id)*)
    } [@@deriving sexp]

let string_of_job job_table i =
  let j = Hashtbl.find job_table i
  in Printf.sprintf "id %d {r:%d;p:%d;p_est:%d;q:%d;u:%d}" i j.r j.p j.p_est j.q j.u

let empty_system maxprocs = { free = maxprocs; running = []; waiting = []; }

let print_system_summary job_table s = 
  Printf.printf 
    "System with %d free resources, %d running jobs, %d waiting jobs .\n" 
    s.free (List.length s.running) (List.length s.waiting);
  Printf.printf "The jobs in running are:\n";
  List.map (fun (_,x) -> Printf.printf "%s\n" (string_of_job job_table x)) s.running;
  Printf.printf "The jobs in waiting are:\n";
  List.map (fun x -> Printf.printf "%s\n" (string_of_job job_table x)) s.waiting;
  Printf.printf "End print system.%s" "\n"


(************************************** Criteria Log *****************************)

type log = (float list) list (*list of (id,sub_time)*)

