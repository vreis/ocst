open System

(************************************** Events ***********************************)
module EventHeap = struct
  type event_type = Submit | Finish [@@deriving show]
  type event = { time : int;
             id : int ;
             event_type : event_type } [@@deriving show]

  module OrderedEvents =
  struct
    type t = event
    let compare e1 e2 = compare e1.time e2.time
  end

  include BatHeap.Make (OrderedEvents)

  let of_period job_table s_old s_new starts_inbetween =
    let h_temp = 
      let submissions = 
        let to_filter = 
          List.append
          (List.map (fun (i,_) -> i) starts_inbetween)
          s_new.waiting 
        in List.filter (fun x -> not (List.mem x s_old.waiting)) to_filter
      and f h i = 
        let j = Hashtbl.find job_table i 
        in add ({time=j.r; id=i; event_type=Submit}:elem) h
      in List.fold_left f empty submissions
    in let f h (t,i)  = 
        let j = Hashtbl.find job_table i 
        in add ({time=t+j.p; id=i; event_type=Finish}:elem) h
    in List.fold_left f h_temp s_old.running

  let of_job_table job_table =
    let f i j h  = add ({time=j.r; id=i; event_type=Submit}:elem) h
    in Hashtbl.fold f job_table empty

  type unloadedEvents = Events of (t * int * (elem list)) | EndSimulation

  let unloadEvents heap =
    if (size heap = 0) then
      EndSimulation
    else
      let firstEvent = find_min heap
      in let rec getEvent h eventList =
        if (size h = 0) then
          Events (h, firstEvent.time, eventList)
        else
          let e = find_min h
          in if e.time > firstEvent.time then
            Events (h, firstEvent.time, eventList)
          else
            getEvent (del_min h) (e::eventList)
      in getEvent (del_min heap) [firstEvent]

  let print_events job_table h = 
    Printf.printf "Printing event heap.\n";
    let f ev = 
      let evt = match ev.event_type with
        | Submit -> "submission"
        | Finish -> "end"
      in Printf.printf "Event with time %d, job id %d, type %s, and job is %s\n" ev.time ev.id evt (System.string_of_job job_table ev.id);
    in List.iter f (to_list h);
    Printf.printf "End print event heap.%s" "\n"
end
